package com.devOpsLab.DOL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevOpsLabApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevOpsLabApplication.class, args);
	}

}
